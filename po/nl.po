# Dutch translation for lomiri-system-settings-security-privacy
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-security-privacy package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-security-privacy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2021-03-21 15:22+0000\n"
"Last-Translator: Heimen Stoffels <vistausss@outlook.com>\n"
"Language-Team: Dutch <https://translate.ubports.com/projects/ubports/"
"system-settings/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: ../plugins/security-privacy/LockSecurity.qml:36
#: ../plugins/security-privacy/PhoneLocking.qml:66
msgid "Lock security"
msgstr "Ontgrendelbeveiliging"

#: ../plugins/security-privacy/PhoneLocking.qml:95
#: ../plugins/security-privacy/PhoneLocking.qml:113
msgid "Never"
msgstr "Nooit"

#: ../plugins/security-privacy/LockSecurity.qml:365
#: ../plugins/security-privacy/SimPin.qml:179
#: ../plugins/security-privacy/SimPin.qml:318
msgid "Cancel"
msgstr "Annuleren"

#: ../plugins/security-privacy/LockSecurity.qml:391
msgid "Set"
msgstr "Instellen"

#: ../plugins/security-privacy/PhoneLocking.qml:89
msgid "Lock when idle"
msgstr "Vergrendelen bij inactiviteit"

#: ../plugins/security-privacy/PhoneLocking.qml:90
msgid "Sleep when idle"
msgstr "Slaapstand bij inactiviteit"

#. TRANSLATORS: %1 is the number of seconds
#: ../plugins/security-privacy/PhoneLocking.qml:98
#, qt-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "Na %1 seconde"
msgstr[1] "Na %1 seconden"

#. TRANSLATORS: %1 is the number of minutes
#: ../plugins/security-privacy/PhoneLocking.qml:102
#: ../plugins/security-privacy/PhoneLocking.qml:110
#, qt-format
msgid "After %1 minute"
msgid_plural "After %1 minutes"
msgstr[0] "Na %1 minuut"
msgstr[1] "Na %1 minuten"

#: ../plugins/security-privacy/AppAccess.qml:75
msgid "Camera"
msgstr "Camera"

#: ../plugins/security-privacy/LockSecurity.qml:389
#: ../plugins/security-privacy/SimPin.qml:187
msgid "Change"
msgstr "Aanpassen"

#: ../plugins/security-privacy/PhoneLocking.qml:138
msgid "Launcher"
msgstr "Starter"

#: ../plugins/security-privacy/PageComponent.qml:180
#: ../plugins/security-privacy/PageComponent.qml:248
msgid "Off"
msgstr "Uit"

#: ../plugins/security-privacy/AppAccess.qml:81
#: ../plugins/security-privacy/Location.qml:34
#: ../plugins/security-privacy/PageComponent.qml:240
msgid "Location"
msgstr "Locatie"

#: ../plugins/security-privacy/AppAccess.qml:29
#: ../plugins/security-privacy/PageComponent.qml:259
msgid "App permissions"
msgstr "App-bevoegdheden"

#: ../plugins/security-privacy/AppAccess.qml:69
msgid "Apps that you have granted access to:"
msgstr "Apps die toegang hebben gekregen tot:"

#: ../plugins/security-privacy/AppAccess.qml:76
msgid "Apps that have requested access to your camera"
msgstr "Apps die toegang hebben gevraagd tot je camera"

#: ../plugins/security-privacy/AppAccess.qml:82
msgid "Apps that have requested access to your location"
msgstr "Apps die toegang hebben gevraagd tot je locatie"

#: ../plugins/security-privacy/AppAccess.qml:87
msgid "Microphone"
msgstr "Microfoon"

#: ../plugins/security-privacy/AppAccess.qml:88
msgid "Apps that have requested access to your microphone"
msgstr "Apps die toegang hebben gevraagd tot je microfoon"

#: ../plugins/security-privacy/AppAccess.qml:102
#, qt-format
msgid "%1/%2"
msgstr "%1/%2"

#: ../plugins/security-privacy/AppAccess.qml:103
msgid "0"
msgstr "0"

#: ../plugins/security-privacy/AppAccess.qml:118
msgid "Apps may also request access to online accounts."
msgstr "Apps kunnen ook toegang vragen tot je online-account(s)."

#: ../plugins/security-privacy/AppAccess.qml:123
msgid "Online Accounts…"
msgstr "Online-accounts…"

#: ../plugins/security-privacy/here-terms.qml:24
msgid "Nokia HERE"
msgstr "Nokia HERE"

#: ../plugins/security-privacy/Location.qml:96
msgid "Let the device detect your location:"
msgstr "Geef het apparaat toegang tot je locatie:"

#: ../plugins/security-privacy/Location.qml:175
msgid "Using GPS only (less accurate)"
msgstr "Via gps (minder nauwkeurig)"

#: ../plugins/security-privacy/Location.qml:176
msgid "Using GPS"
msgstr "Via gps"

#: ../plugins/security-privacy/Location.qml:188
#, qt-format
msgid ""
"Using GPS, anonymized Wi-Fi and cellular network info.<br>By selecting this "
"option you accept the <a href='%1'>Nokia HERE terms and conditions</a>."
msgstr ""
"Via gps, geanonimiseerde wifi- en mobiele netwerkinformatie.<br>Als je deze "
"optie kiest, ga je akkoord met de <a href='%1'>algemene voorwaarden van "
"Nokia HERE</a>."

#: ../plugins/security-privacy/Location.qml:189
#, qt-format
msgid ""
"Using GPS and anonymized Wi-Fi info.<br>By selecting this option you accept "
"the <a href='%1'>Nokia HERE terms and conditions</a>."
msgstr ""
"Via gps en geanonimiseerde wifi-informatie.<br>Als je deze optie kiest, ga "
"je akkoord met de <a href='%1'>algemene voorwaarden van Nokia HERE</a>."

#: ../plugins/security-privacy/Location.qml:195
msgid "Not at all"
msgstr "Niet gebruiken"

#: ../plugins/security-privacy/Location.qml:212
msgid ""
"Uses GPS to detect your rough location. When off, GPS turns off to save "
"battery."
msgstr ""
"GPS wordt gebruikt om je locatie bij benadering te bepalen. Schakel gps uit "
"om het accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:214
msgid ""
"Uses WiFi and GPS to detect your rough location. Turning off location "
"detection saves battery."
msgstr ""
"GPS en wifi worden gebruikt om je locatie bij benadering te bepalen. Schakel "
"locatiebepaling uit om het accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:216
msgid ""
"Uses WiFi (currently off) and GPS to detect your rough location. Turning off "
"location detection saves battery."
msgstr ""
"GPS en wifi (momenteel uit) worden gebruikt om je locatie bij benadering te "
"bepalen. Schakel locatiebepaling uit om het accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:218
msgid ""
"Uses WiFi, cell tower locations, and GPS to detect your rough location. "
"Turning off location detection saves battery."
msgstr ""
"GPS, wifi en mobiele zendmasten worden gebruikt om je locatie bij benadering "
"te bepalen. Schakel locatiebepaling uit om het accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:220
msgid ""
"Uses WiFi, cell tower locations (no current cellular connection), and GPS to "
"detect your rough location. Turning off location detection saves battery."
msgstr ""
"Wifi, mobiele zendmasten (momenteel geen mobiele verbinding) en gps worden "
"gebruikt om je locatie bij benadering te bepalen. Schakel locatiebepaling "
"uit om het accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:222
msgid ""
"Uses WiFi (currently off), cell tower locations, and GPS to detect your "
"rough location. Turning off location detection saves battery."
msgstr ""
"Wifi (momenteel uit), mobiele zendmasten en gps worden gebruikt om je "
"locatie bij benadering te bepalen. Schakel locatiebepaling uit om het "
"accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:224
msgid ""
"Uses WiFi (currently off), cell tower locations (no current cellular "
"connection), and GPS to detect your rough location. Turning off location "
"detection saves battery."
msgstr ""
"Wifi (momenteel uit), mobiele zendmasten (momenteel geen mobiele verbinding) "
"en gps worden gebruikt om je locatie bij benadering te bepalen. Schakel "
"locatiebepaling uit om het accuverbruik te verminderen."

#: ../plugins/security-privacy/Location.qml:231
msgid "Let apps access this location:"
msgstr "Geef apps toegang tot deze locatie:"

#: ../plugins/security-privacy/Location.qml:254
msgid "None requested"
msgstr "Geen aanvragen"

#: ../plugins/security-privacy/LockSecurity.qml:137
#: ../plugins/security-privacy/LockSecurity.qml:563
msgid "Change passcode…"
msgstr "Toegangscode wijzigen…"

#: ../plugins/security-privacy/LockSecurity.qml:139
#: ../plugins/security-privacy/LockSecurity.qml:564
msgid "Change passphrase…"
msgstr "Wachtwoordzin wijzigen…"

#: ../plugins/security-privacy/LockSecurity.qml:146
msgid "Switch to swipe"
msgstr "Vegen om te ontgrendelen"

#: ../plugins/security-privacy/LockSecurity.qml:148
msgid "Switch to passcode"
msgstr "Toegangscode om te ontgrendelen"

#: ../plugins/security-privacy/LockSecurity.qml:150
msgid "Switch to passphrase"
msgstr "Wachtwoordzin om te ontgrendelen"

#: ../plugins/security-privacy/LockSecurity.qml:159
msgid "Existing passcode"
msgstr "Huidige toegangscode"

#: ../plugins/security-privacy/LockSecurity.qml:161
msgid "Existing passphrase"
msgstr "Huidige wachtwoordzin"

#: ../plugins/security-privacy/LockSecurity.qml:231
msgid "Choose passcode"
msgstr "Kies een toegangscode"

#: ../plugins/security-privacy/LockSecurity.qml:233
msgid "Choose passphrase"
msgstr "Kies een wachtwoordzin"

#: ../plugins/security-privacy/LockSecurity.qml:291
msgid "Confirm passcode"
msgstr "Toegangscode bevestigen"

#: ../plugins/security-privacy/LockSecurity.qml:293
msgid "Confirm passphrase"
msgstr "Wachtwoordzin bevestigen"

#: ../plugins/security-privacy/LockSecurity.qml:348
msgid "Those passcodes don't match. Try again."
msgstr "De toegangscodes komen niet overeen. Probeer het opnieuw."

#: ../plugins/security-privacy/LockSecurity.qml:351
msgid "Those passphrases don't match. Try again."
msgstr "De wachtwoordzinnen komen niet overeen. Probeer het opnieuw."

#: ../plugins/security-privacy/LockSecurity.qml:386
msgid "Unset"
msgstr "Niet ingesteld"

#: ../plugins/security-privacy/LockSecurity.qml:463
msgid "Unlock the device using:"
msgstr "Apparaat ontgrendelen middels:"

#: ../plugins/security-privacy/LockSecurity.qml:467
msgid "Swipe (no security)"
msgstr "Vegen (onbeveiligd)"

#: ../plugins/security-privacy/LockSecurity.qml:468
msgid "4-digit passcode"
msgstr "Viercijferige toegangscode"

#: ../plugins/security-privacy/LockSecurity.qml:469
#: ../plugins/security-privacy/PhoneLocking.qml:62
msgid "Passphrase"
msgstr "Wachtwoordzin"

#: ../plugins/security-privacy/LockSecurity.qml:470
#: ../plugins/security-privacy/PhoneLocking.qml:63
msgid "Fingerprint"
msgstr "Vingerafdruk"

#: ../plugins/security-privacy/LockSecurity.qml:471
msgid "Swipe (no security)… "
msgstr "Vegen (onbeveiligd)… "

#: ../plugins/security-privacy/LockSecurity.qml:472
msgid "4-digit passcode…"
msgstr "Viercijferige toegangscode…"

#: ../plugins/security-privacy/LockSecurity.qml:473
msgid "Passphrase…"
msgstr "Wachtwoordzin…"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../plugins/security-privacy/PageComponent.qml:39 ../build/po/settings.js:306
msgid "Security & Privacy"
msgstr "Beveiliging en privacy"

#: ../plugins/security-privacy/PageComponent.qml:111
msgid "Phone and Internet"
msgstr "Telefoon en internet"

#: ../plugins/security-privacy/PageComponent.qml:113
msgid "Phone only"
msgstr "Alleen telefoon"

#: ../plugins/security-privacy/PageComponent.qml:137
msgid "Security"
msgstr "Beveiliging"

#: ../plugins/security-privacy/PageComponent.qml:143
msgid "Fingerprint ID"
msgstr "Vingerafdruk"

#: ../plugins/security-privacy/PageComponent.qml:163
#: ../plugins/security-privacy/PhoneLocking.qml:32
msgid "Locking and unlocking"
msgstr "Vergrendelen en ontgrendelen"

#: ../plugins/security-privacy/PageComponent.qml:173
#: ../plugins/security-privacy/SimPin.qml:37
#: ../plugins/security-privacy/SimPin.qml:398
msgid "SIM PIN"
msgstr "Simkaartpincode"

#: ../plugins/security-privacy/PageComponent.qml:176
#: ../plugins/security-privacy/PageComponent.qml:248
msgid "On"
msgstr "Aan"

#: ../plugins/security-privacy/PageComponent.qml:187
msgid "Encryption"
msgstr "Versleuteling"

#: ../plugins/security-privacy/PageComponent.qml:197
msgid ""
"Encryption protects against access to phone data when the phone is connected "
"to a PC or other device."
msgstr ""
"Versleuteling beschermt je telefoongegevens tegen ongewenste toegang als je "
"telefoon verbonden is met een pc of ander apparaat."

#: ../plugins/security-privacy/PageComponent.qml:202
msgid "Privacy"
msgstr "Privacy"

#: ../plugins/security-privacy/PageComponent.qml:206
msgid "Stats on welcome screen"
msgstr "Statistieken op welkomstscherm"

#: ../plugins/security-privacy/PageComponent.qml:216
msgid "Messages on welcome screen"
msgstr "Berichten op welkomstscherm"

#: ../plugins/security-privacy/PhoneLocking.qml:60
msgctxt "Unlock with swipe"
msgid "None"
msgstr "Geen"

#: ../plugins/security-privacy/PhoneLocking.qml:61
msgid "Passcode"
msgstr "Toegangscode"

#: ../plugins/security-privacy/PhoneLocking.qml:126
msgid "Sleep locks immediately"
msgstr "Slaapstand vergrendelt meteen"

#: ../plugins/security-privacy/PhoneLocking.qml:132
msgid "When locked, allow:"
msgstr "Toestaan bij vergrendeling:"

#: ../plugins/security-privacy/PhoneLocking.qml:152
msgid "Notifications and quick settings"
msgstr "Toegang tot meldingen en snelle instellingen"

#: ../plugins/security-privacy/PhoneLocking.qml:167
msgid "Turn on lock security to restrict access when the device is locked."
msgstr ""
"Schakel vergrendelbeveiliging in om toegang tot het apparaat te blokkeren "
"als het vergrendeld is."

#: ../plugins/security-privacy/PhoneLocking.qml:168
msgid "Other apps and functions will prompt you to unlock."
msgstr "Andere apps en functies zullen je vragen om ontgrendeling."

#: ../plugins/security-privacy/SimPin.qml:48
msgid "Change SIM PIN"
msgstr "Simpincode wijzigen"

#: ../plugins/security-privacy/SimPin.qml:53
#: ../plugins/security-privacy/SimPin.qml:219
#, qt-format
msgid "Incorrect PIN. %1 attempt remaining."
msgid_plural "Incorrect PIN. %1 attempts remaining."
msgstr[0] "Onjuiste pincode - nog %1 poging."
msgstr[1] "Onjuiste pincode - nog %1 pogingen."

#: ../plugins/security-privacy/SimPin.qml:58
#: ../plugins/security-privacy/SimPin.qml:120
#: ../plugins/security-privacy/SimPin.qml:224
#: ../plugins/security-privacy/SimPin.qml:297
msgid "No more attempts allowed"
msgstr "Geen pogingen meer toegestaan"

#: ../plugins/security-privacy/SimPin.qml:93
msgid "Current PIN:"
msgstr "Huidige pincode:"

#: ../plugins/security-privacy/SimPin.qml:115
#: ../plugins/security-privacy/SimPin.qml:292
#, qt-format
msgid "%1 attempt allowed."
msgid_plural "%1 attempts allowed."
msgstr[0] "%1 poging toegestaan."
msgstr[1] "%1 pogingen toegestaan."

#: ../plugins/security-privacy/SimPin.qml:135
msgid "Choose new PIN:"
msgstr "Voer een nieuwe pincode in:"

#: ../plugins/security-privacy/SimPin.qml:146
msgid "Confirm new PIN:"
msgstr "Bevestig de nieuwe pincode:"

#: ../plugins/security-privacy/SimPin.qml:168
msgid "PINs don't match. Try again."
msgstr "De pincodes komen niet overeen. Probeer het opnieuw."

#: ../plugins/security-privacy/SimPin.qml:213
msgid "Enter SIM PIN"
msgstr "Voer de simpincode in"

#: ../plugins/security-privacy/SimPin.qml:214
msgid "Enter Previous SIM PIN"
msgstr "Voer de oude simpincode in"

#: ../plugins/security-privacy/SimPin.qml:334
msgid "Unlock"
msgstr "Ontgrendelen"

#: ../plugins/security-privacy/SimPin.qml:334
msgid "Lock"
msgstr "Vergrendelen"

#: ../plugins/security-privacy/SimPin.qml:413
msgid "Unlocked"
msgstr "Deblokkeren"

#: ../plugins/security-privacy/SimPin.qml:416
msgid "Change PIN…"
msgstr "Pincode wijzigen…"

#: ../plugins/security-privacy/SimPin.qml:427
msgid "Locked"
msgstr "Geblokkeerd"

#: ../plugins/security-privacy/SimPin.qml:431
msgid "Unlock…"
msgstr "Deblokkeren…"

#: ../plugins/security-privacy/SimPin.qml:445
msgid ""
"When a SIM PIN is set, it must be entered to access cellular services after "
"restarting the device or swapping the SIM."
msgstr ""
"Als er een simpincode is ingesteld, moet deze telkens worden ingevoerd na "
"het herstarten of verwisselen van de simkaart, om gebruik te kunnen maken "
"van mobiele diensten."

#: ../plugins/security-privacy/SimPin.qml:449
msgid "Entering an incorrect PIN repeatedly may lock the SIM permanently."
msgstr ""
"Als u meermaals een onjuiste pincode invoert, kan de simkaart mogelijk "
"definitief geblokkeerd worden."

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:70
msgid "lock"
msgstr "vergrendelen"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:138
msgid "sim"
msgstr "simkaart"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:308
msgid "security"
msgstr "beveiliging"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:310
msgid "privacy"
msgstr "privacy"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:316
msgid "pin"
msgstr "pin"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:318
msgid "code"
msgstr "code"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:320
msgid "password"
msgstr "wachtwoord"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:322
msgid "passphrase"
msgstr "wachtwoordzin"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:324
msgid "swipe"
msgstr "vegen"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:326
msgid "allow"
msgstr "toestaan"

#. TRANSLATORS: This is a keyword or name for the security-privacy plugin which is used while searching
#: ../build/po/settings.js:328
msgid "access"
msgstr "toegang"

#: ../plugins/security-privacy/securityprivacy.cpp:369
msgid "Incorrect passcode. Try again."
msgstr "Onjuiste toegangscode. Probeer het opnieuw."

#: ../plugins/security-privacy/securityprivacy.cpp:371
msgid "Incorrect passphrase. Try again."
msgstr "Onjuiste wachtwoordzin. Probeer het opnieuw."

#: ../plugins/security-privacy/securityprivacy.cpp:374
msgid "Could not set security mode"
msgstr "Kan beveiligingsmodus niet instellen"

#: ../plugins/security-privacy/securityprivacy.cpp:399
msgid "Could not set security display hint"
msgstr "Kan beveiligingshint op scherm niet instellen"

#: ../plugins/security-privacy/securityprivacy.cpp:413
msgid "Authentication token manipulation error"
msgstr "Fout tijdens manipulatie van authenticatiemiddel"

